SSH_HOSTNAME = taskforce

STATIC_ROOT = static-dist/
STATIC_TARGET = /home/www/static/
REMOTE_DJANGO_ROOT = /home/www/taskforce-django


serve:
	make -j4 serve-django serve-telegram

serve-django:
	. `pwd`/.env/bin/activate; python manage.py runserver 23000

serve-telegram:
	sleep 1; cd telegram; . `pwd`/.env/bin/activate; make run

populate:
	. `pwd`/.env/bin/activate; python manage.py populate

mockdata:
	. `pwd`/.env/bin/activate; python manage.py remove_test_data; python manage.py insert_test_data

install:
	virtualenv .env --python=python3; \
	. `pwd`/.env/bin/activate; pip install -r requirements.txt; python manage.py migrate

deploy:
	. `pwd`/.env/bin/activate; python manage.py collectstatic --no-input
	rsync -arvu --delete $(STATIC_ROOT) $(SSH_HOSTNAME):$(STATIC_TARGET)
	git push production master

flush:
	. `pwd`/.env/bin/activate; python manage.py flush && python manage.py migrate

reset-db: flush
	. `pwd`/.env/bin/activate; python manage.py populate

createsuperuser:
	. `pwd`/.env/bin/activate; python manage.py createsuperuser

migrations:
	. `pwd`/.env/bin/activate; python manage.py makemigrations && python manage.py migrate

shell:
	. `pwd`/.env/bin/activate; python manage.py shell

