// REALLY important, Django will break with CSRF errors if we don't set this option
// https://github.com/pagekit/vue-resource/blob/master/docs/config.md#legacy-web-servers
Vue.http.options.emulateJSON = true;

function insertHtmlAtCursor(html) {
    // https://stackoverflow.com/a/22105936/122400
    var range, node;
    if (window.getSelection && window.getSelection().getRangeAt) {
        range = window.getSelection().getRangeAt(0);
        console.log(range);
        node = range.createContextualFragment(html);
        range.insertNode(node);
        window.getSelection().collapseToEnd();
        //window.getSelection().modify('move', 'forward', 'character');
    } else if (document.selection && document.selection.createRange) {
        document.selection.createRange().pasteHTML(html);
        document.selection.collapseToEnd();
        document.selection.modify('move', 'forward', 'character');
    }
}

var vm = new Vue({
  el: '#question-editor',
  name: 'questionWidget',
  delimiters: ['${', '}'],
  data: {
    question_bits: []
  },

  beforeMount: function() {
    this.$http.get(getQuestionURL).then(function (response) {
      this.question_bits = JSON.parse(response.bodyText); 
    }, function (response) {
      console.log("Error in beforeMount!");
      console.log(response);
    });
  },

  mounted: function() {
    console.log(this.$el.querySelector('button'));
  },

  methods: {
    removeQuestionBit: function(index) {
      this.question_bits.splice(index, 1);
    },
    addQuestionBit: function() {
      this.question_bits.unshift({ type: "text", text: "New line" });
    },

    ceEdit: function(ev, target, property) {
      // edit ContentEditable element
      target[property] = ev.target.innerText.replace(/\n/g, ' ');
    },
    cePressEnter: function(ev, target, property) {
      // press enter in ContentEditable element = save
      ev.preventDefault();
      this.ceEdit(ev, target, property);
      ev.target.blur();
    },
    ceButtonEdit: function(ev, choice) {
      choice.label = ev.target.innerText;
    },
    ceButtonPressEnter: function(ev, choice) {
      ev.preventDefault();
      choice.label = ev.target.innerText.replace(/\n/g, ' ');
      ev.target.blur();
    },
    captureSpace: function(ev) {
      console.log(ev);
    },
    buttonClicked: function(ev) {
      // this deals with the Space key firing the onClick event
      // we want it to add an actual space
      // FIXME: multiple spaces don't work
      ev.preventDefault();
      if (!ev.x && !ev.y && !ev.clientX && !ev.clientY) {
        // it's a space press, not a mouse click
        insertHtmlAtCursor(' ');
      }
    }

    /*
    updateQuestionText: function(ev) {
      // click Update in question text form
      ev.preventDefault();
      this.$refs.questionFormStatus.innerText = "Saving...";
      // catch form input values -- this ensures we also submit the csrf_token field
      var postdata = {};
      for (var i=0; i<ev.target.elements.length; i++) {
        var el = ev.target.elements[i];
        if (el.name) {
          postdata[el.name] = el.value;
        }
      }
      this.$http.post(setQuestionURL, postdata).then(function (response) {
        this.$refs.questionFormStatus.innerText = "Saved!";
      }, function (response) {
        this.$refs.questionFormStatus.innerText = "Error when saving :-(";
      });
    }
    */
  }
});
