// REALLY important, Django will break with CSRF errors if we don't set this option
// https://github.com/pagekit/vue-resource/blob/master/docs/config.md#legacy-web-servers
Vue.http.options.emulateJSON = true;

var vm = new Vue({
  el: '#intro-editor',
  name: 'introWidget',
  delimiters: ['${', '}'],
  data: {
    intro_bits: []
  },

  beforeMount: function() {
    this.$http.get(getIntroURL).then(function (response) {
      this.intro_bits = JSON.parse(response.bodyText); 
    }, function (response) {
      console.log("Error in beforeMount!");
      console.log(response);
    });
  },

  methods: {
    removeIntroBit: function(index) {
      this.intro_bits.splice(index, 1);
    },
    addIntroBit: function() {
      this.intro_bits.unshift({ type: "text", text: "New line" });
    },

    ceEdit: function(ev, target, property) {
      // edit ContentEditable element
      target[property] = ev.target.innerText.replace(/\n/g, ' ');
    },
    cePressEnter: function(ev, target, property) {
      // press enter in ContentEditable element = save
      ev.preventDefault();
      this.ceEdit(ev, target, property);
      ev.target.blur();
    },
    ceButtonEdit: function(ev, choice) {
      choice.label = ev.target.innerText;
    },
    ceButtonPressEnter: function(ev, choice) {
      ev.preventDefault();
      choice.label = ev.target.innerText.replace(/\n/g, ' ');
      ev.target.blur();
    },

    /*
    updateIntroText: function(ev) {
      // click Update in intro text form
      ev.preventDefault();
      this.$refs.introFormStatus.innerText = "Saving...";
      // catch form input values -- this ensures we also submit the csrf_token field
      var postdata = {};
      for (var i=0; i<ev.target.elements.length; i++) {
        var el = ev.target.elements[i];
        if (el.name) {
          postdata[el.name] = el.value;
        }
      }
      this.$http.post(setIntroURL, postdata).then(function (response) {
        this.$refs.introFormStatus.innerText = "Saved!";
      }, function (response) {
        this.$refs.introFormStatus.innerText = "Error when saving :-(";
      });
    }
    */
  }
});
