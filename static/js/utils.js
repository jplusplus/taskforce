function guid() {
  // return a pseudo-unique ID
  // http://stackoverflow.com/a/105074/122400
  function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
  }
  return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
    s4() + '-' + s4() + s4() + s4();
}

var md = window.markdownit().use(window.markdownitEmoji);

$.renderMarkdown = function (txt, skipEmoji, onlyInner) {
  // Renders a Markdown formatted string into a jQuery object.
  var $content = $($.parseHTML(md.render(txt).trim()));
  if (!skipEmoji) {
  $content.Emoji({
		path:  '/static/images/emojione-40/',
		class: 'emoji',
		ext:   'png'
	});
  }
  if (onlyInner) {
    if ($content.length) {
      $content = $content[0].innerHTML;
    } else {
      $content = "";
    }
  }
  return $content;
};
