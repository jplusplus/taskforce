// REALLY important, Django will break with CSRF errors if we don't set this option
// https://github.com/pagekit/vue-resource/blob/master/docs/config.md#legacy-web-servers
Vue.http.options.emulateJSON = true;

function insertHtmlAtCursor(html) {
    // https://stackoverflow.com/a/22105936/122400
    var range, node;
    if (window.getSelection && window.getSelection().getRangeAt) {
        range = window.getSelection().getRangeAt(0);
        console.log(range);
        node = range.createContextualFragment(html);
        range.insertNode(node);
        window.getSelection().collapseToEnd();
        //window.getSelection().modify('move', 'forward', 'character');
    } else if (document.selection && document.selection.createRange) {
        document.selection.createRange().pasteHTML(html);
        document.selection.collapseToEnd();
        document.selection.modify('move', 'forward', 'character');
    }
}




var vm = new Vue({
  el: '#tutorial-editor',
  name: 'tutorialWidget',
  delimiters: ['${', '}'],
  data: {
    tutorial_bits: []
  },

  beforeMount: function() {
    this.$http.get(getTutorialURL).then(function (response) {
      this.tutorial_bits = JSON.parse(response.bodyText); 
    }, function (response) {
      console.log("Error in beforeMount!");
      console.log(response);
    });
    $('#image-upload-form').toggle();
  },

  mounted: function() {
    console.log(this.$el.querySelector('button'));
  },

  methods: {
    removeBit: function(ev, index) {
      ev.preventDefault();
      this.tutorial_bits.splice(index, 1);
    },
    addTextBit: function() {
      this.tutorial_bits.unshift({ type: "text", text: "New line" });
    },
    addQuestionChoice: function(bit) {
      bit.choices.push({
        "label": "Another choice",
        "next": {
          "type": "text",
          "text": "You selected this choice, good!"
        }
      });
    },
    renderMarkdown: function(text) {
      return jQuery.renderMarkdown(text, false, true);
    },
    removeQuestionChoice: function(ev, bit, choice) {
      ev.preventDefault();
      bit.choices = bit.choices.filter( function(el) {
        return el.label !== choice.label;
      });
    },
    addQuestionBit: function() {
      this.tutorial_bits.unshift({ 
        type: "question-tutorial", 
        text: "What's the right answer?",
        choices: [
            {
                "label": "Right",
                "next": {
                    "type": "text",
                    "text": "Yes! You picked the right answer!"
                }
            },
            {
                "label": "Wrong",
                "next": {
                    "type": "text",
                    "text": "No, 'Right' was the correct answer."
                }
            }
        ],
      });
    },

    addImage: function(bit) {
      $input = $('#image-upload-form').find('input[name="file_source"]');
      // set up form to handle submit event differently
      $('#image-upload-form').submit(function (event) {
        event.preventDefault();
        var data = new FormData($('#image-upload-form').get(0));
        $.ajax({
          url: $(this).attr('action'),
          type: $(this).attr('method'),
          data: data,
          cache: false,
          processData: false,
          contentType: false,
          success: function(data) {
            console.log("success!");
            if (bit.type == 'text') {
              bit.type = 'image';
            }
            bit.media_url = data.url;
            // FIXME hack: we have to refresh the text field for the var to update
            bit.text = bit.text + " ";
            bit.text = bit.text.trim();
          }
        });
        return false;
      });
      // when image field changes (file selected), submit form
      $input.change( function() {
        $('#image-upload-form').submit();
      });

      // now, simulate click on file upload field for user to pick image
      $input.click();
    },
    removeImage: function(bit) {
      delete bit.media_url;
      if (bit.type == 'image') {
        bit.type = 'text';
      }
      // FIXME hack: we have to refresh the text field for the var to update
      bit.text = bit.text + " ";
      bit.text = bit.text.trim();
    },



    ceEdit: function(ev, target, property) {
      // edit ContentEditable element
      target[property] = ev.target.innerText.replace(/\n/g, ' ');
    },
    cePressEnter: function(ev, target, property) {
      // press enter in ContentEditable element = save
      ev.preventDefault();
      this.ceEdit(ev, target, property);
      ev.target.blur();
    },
    ceButtonEdit: function(ev, choice) {
      choice.label = ev.target.innerText;
    },
    ceButtonPressEnter: function(ev, choice) {
      ev.preventDefault();
      choice.label = ev.target.innerText.replace(/\n/g, ' ');
      ev.target.blur();
    },
    captureSpace: function(ev) {
      console.log(ev);
    },
    buttonClicked: function(ev) {
      // this deals with the Space key firing the onClick event
      // we want it to add an actual space
      // FIXME: multiple spaces don't work
      ev.preventDefault();
      if (!ev.x && !ev.y && !ev.clientX && !ev.clientY) {
        // it's a space press, not a mouse click
        insertHtmlAtCursor(' ');
      }
    }

    /*
    updateTutorialText: function(ev) {
      // click Update in tutorial text form
      ev.preventDefault();
      this.$refs.tutorialFormStatus.innerText = "Saving...";
      // catch form input values -- this ensures we also submit the csrf_token field
      var postdata = {};
      for (var i=0; i<ev.target.elements.length; i++) {
        var el = ev.target.elements[i];
        if (el.name) {
          postdata[el.name] = el.value;
        }
      }
      this.$http.post(setTutorialURL, postdata).then(function (response) {
        this.$refs.tutorialFormStatus.innerText = "Saved!";
      }, function (response) {
        this.$refs.tutorialFormStatus.innerText = "Error when saving :-(";
      });
    }
    */
  }
});
