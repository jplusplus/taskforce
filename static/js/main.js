var messageEndpointURL;
var questionEndpointURL;
var introEndpointURL;
var obfuscateInput = false;

// figure out if we're on local or production and get values from settings.js
var base_url;
var currentURL = window.location.href;
if (currentURL.indexOf('jplusplus.org') !== -1) {
  console.log('production');
  base_url = api_base_url_production;
} else {
  console.log('local');
  base_url = api_base_url_local;
}
messageEndpointURL = base_url + bot_name + "/text/";
questionEndpointURL = base_url + bot_name + "/a/";
introEndpointURL = base_url + bot_name + "/get-intro/";

var waitingForAnswer = false;

// Django CSRF protection
function getCookie(name) {
  var cookieValue = null;
  if (document.cookie && document.cookie !== '') {
    var cookies = document.cookie.split(';');
    for (var i = 0; i < cookies.length; i++) {
      var cookie = jQuery.trim(cookies[i]);
      // Does this cookie string begin with the name we want?
      if (cookie.substring(0, name.length + 1) === (name + '=')) {
	cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
	break;
      }
    }
  }
  return cookieValue;
}
var csrftoken = getCookie('csrftoken');
function csrfSafeMethod(method) {
  // these HTTP methods do not require CSRF protection
  return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}
$.ajaxSetup({
  beforeSend: function(xhr, settings) {
    if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
      xhr.setRequestHeader("X-CSRFToken", csrftoken);
    }
  }
});


function prepareInputBox() {
  // create a GUID for the new message
  $('#msgid-field').val(guid());

  // focus the input box
  $('#input-box')
    .removeAttr('disabled')
    .val('')
    .focus();
}

function createUserMessage(content, msgid) {
  // text only for now
  var $msgbox = $('<div>', { 'class': 'message-box user not-received' }) // will lose the not-received class on server response
    .attr('id', msgid);
  var $container = $('<div>', { 'class': 'message' });
  $msgbox.append($container);
  var $message = $.renderMarkdown(content);
  if (obfuscateInput) {
    $container.append($('<p>', { class: 'hidden-password', text: 'xxxxxxxx' }));
  } else {
    $container.append($message);
  }
  $('#chat-area').append($msgbox)
    .animate({scrollTop: $('#chat-area').prop('scrollHeight')});
}

function createBotMessage(bit) {
  // console.log(bit);
  var $msgbox = $('<div>', {
    'class': 'message-box bot'
  });
  var $container = $('<div>', {
    'class': 'message'
  });
  $msgbox.append($container);
  // TODO: be more elegant handling images
  if (bit.media_url) {
    // image
    var $reply = $('<p>', { 'class': 'image' });
    $img = $('<img>', { 'src': bit.media_url });
    $reply.append($img);
    $container.append($reply);
    if (bit.text) {
      $container.append($('<p>', { text: bit.text }));
    }
  } else {
    // text
    $container.append($.renderMarkdown(bit.text));
  }

  if (bit.choices) {
    addChoicesToContainer($container, bit);
  }
  if (bit.expects) {
    // this question expects an answer in the form of a user message
    $('#questionid-field').val(bit.id);
    // also focus on the text box
    $('#input-box')
      .removeAttr('disabled')
      .val('')
      .focus();
  }
  $('#chat-area').append($msgbox)
    .animate({scrollTop: $('#chat-area').prop('scrollHeight')});
}

function addChoicesToContainer($container, bit) {
  var $button_group = $('<div>', {
    'class': 'button-group stacked-for-small expand'
  });
  $container.append($button_group);
  $.each(bit.choices, function(idx, c) {
    // the API response includes an array of choices
    // we go through them to add each as a <button> element
    var value = c.value;
    var label = c.label;
    var answer_id = 'answer-' + bit.id + '-' + value;
    var $button = $('<button>', {
      html: $.renderMarkdown(label, false, true),
      id: answer_id,
      'class': 'button small',
      click: function() {
        // user clicked a choice; first, disable the buttons
        $(this).addClass('user-answer');
        $parent = $(this).parent();
        $parent.addClass('inactive');
        $parent.find('button').attr('disabled', '');

        // disable the waitingForAnswer flag for tutorial questions
        waitingForAnswer = false;

        if (!c.next) {
          // if it's a simple answer with a value, 
          // send it to the proper API endpoint
          var apipost = $.post(questionEndpointURL, {
            question_id: bit.id,
            answer_value: value
          });
          apipost.done(function(rdata) {
            // answer successfully received and processed, display confirmation reply
            processResponseData(rdata);
          });
        } else {
          // if the choice has a "next" field, it means it should ask an additional question
          // console.log(c.next.text);
          createBotMessage({
            type: c.next.type,
            text: c.next.text,
            expects: c.next.expects,
            id: bit.id
          });
        }
      }
    });
    $button_group.append($button);
  });
}


function loopOverData(data) {
  // called in processResponseData to go through each response bit,
  // waiting for user input in the case of tutorial questions
  setTimeout(function () {
    if ( !waitingForAnswer ) {
      bit = data.bits.shift();
      createBotMessage(bit);  
      if (bit.type === "question-tutorial") {
        waitingForAnswer = true;
      }
      if (data.bits.length) {
        loopOverData(data);
      }
    } else {
      loopOverData(data);
    }
  }, 300);
}


function processResponseData(data) {
  if (data.csrf_token) {
    csrftoken = data.csrf_token;
    // change the form token
    $('[name=csrfmiddlewaretoken]').attr('value', csrftoken);
    // TODO: refactor
    $.ajaxSetup({
      beforeSend: function(xhr, settings) {
        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
          xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
      }
    });
  }

  var state_checked = false;
  $.each(data.extra_fields, function(field, value) {
    var $input = $('<input>', { 'type': 'hidden', 'name': field, 'value': value });
    $('#extra-fields').append($input);
    state_checked = true;
    // console.log(field, value);
    if (field === "state" && value.indexOf('password') != -1) {
      obfuscateInput = true;
      $('#input-box').attr('type', 'password');
      console.log(value);
    } else if (field === "state" && value.indexOf('password') == -1) {
      // reset obfuscation
      obfuscateInput = false;
      $('#input-box').attr('type', 'text');
    }
  });
  if (!state_checked) {
    // reset obfuscation
    obfuscateInput = false;
    $('#input-box').attr('type', 'text');
  }

  loopOverData(data);
}

function onMessageEntered(data) {
  // Called on form submission, before the Ajax request is sent.
  
  // echo the user message in the chat area immediately
  createUserMessage(data[1].value, data[2].value);

  // disable the input box until we hear back (we re-enable it in the 'success' event)
  $('#input-box').attr('disabled', '');
}

function onMessageResponse(data) {
  // Called when we get a response after sending a message.

  // mark user message as received
  $('#' + data.replying_to).removeClass('not-received');

  // reset question id field
  if ($('#questionid-field').val()) {
    $('#questionid-field').val('');
  }
  // reset extra fields
  $('#extra-fields').empty();

  // now vee must deel vidit
  processResponseData(data);



  // re-activate input box and move focus to it
  prepareInputBox();
}

function onFormError(data, status, err) {
  createBotMessage({"type": "text", "text": "Ouch, got an error when trying to contact the Taskforce servers. Sorry for this :("});
  console.log(data.status);
  console.log(data.responseText);
  prepareInputBox();
}

function getIntro() {
  $.get(introEndpointURL, function(data) {
    processResponseData(data);
  })
  .fail(function() {
    createBotMessage({"text": "Oh my, I can't contact the Taskforce servers. You'll have to try sometime later. I'm really sorry for this."});
    prepareInputBox();
  });
}

$(function() {

  // start by moving focus to the input box
  prepareInputBox();

  // get the intro texts
  getIntro();

  // ajaxForm comes from the jQuery form plugin
  $('#input-form').ajaxForm({
    url: messageEndpointURL,
    dataType:  'json', 
    clearForm: true, // clear the form after successful submission
    beforeSubmit: onMessageEntered,
    success: onMessageResponse,
    error: onFormError,
    timeout: 3000,
  });

  // set up login and register shortcuts
  $('#login-shortcut').click(function() {
    $('#input-box').val('login');
    $('#input-form').submit();
  });
  $('#register-shortcut').click(function() {
    $('#input-box').val('register');
    $('#input-form').submit();
  });
});
