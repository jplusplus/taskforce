
// http://stackoverflow.com/questions/10025860/simple-stylesheet-switcher-using-jquery-and-a-dropdown-menu
function changeTheme(name) { 
  var theme_name = name;
  if (!name) {
    // no name specified, get it from dropdown
    theme_name = $('#theme-picker').val();
  }
  var theme_path = 'css/' + theme_name + '-style.css';
  // set stylesheet
  $('#theme-link').attr('href', theme_path);
  // change combobox to current stylesheet
  $('#theme-picker').val(theme_name);
  // get url without params - http://stackoverflow.com/a/6257480/122400
  var current_url = location.protocol + '//' + location.host + location.pathname;
  // get theme name
  history.replaceState('', 
                       $(document).find("title").text(), 
                       current_url + '?style=' + theme_name);
}

$.urlParam = function(name){
  var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
  if (!results) {
    return;
  } else {
    return results[1] || 0;
  }
};

$(function() {
  if ($.urlParam('style')) {
    changeTheme($.urlParam('style'));
  }
});

// Show/Hide dropdown with Ctrl+RightArrow
// https://stackoverflow.com/questions/11063588/javascript-toggle-on-keydown#11063638
$(document).keydown(function (e) {
// 39 = right arrow
  if (e.keyCode == 39 && e.ctrlKey) {
       $("#theme-picker").toggle();
  }
});
