
Install
=======

## PostgreSQL

First we need to install PostgreSQL on the system and create a user.

On Debian, do these commands:

```bash
sudo aptitude install postgresql postgresql-client
sudo -u postgres bash
createuser taskforceuser
createdb -O taskforceuser taskforce
psql
```

The last command will put you in the PostgreSQL shell. There, do this command, altering the password to another for your system:

```sql
alter user taskforceuser with password 'taskforce';
```

And type `exit` to leave the PostgreSQL shell, and `exit` again to leave the postgres shell session.

Now, copy the file `tfcore/settings_secrets.py.sample` to `tfcore/settings_secrets.py` and alter the database password to the one you set.

## Install Taskforce

* run `make install`
* run `make reset-db`

Run
===

* run `make serve`
