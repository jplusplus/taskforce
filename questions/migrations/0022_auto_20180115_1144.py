# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2018-01-15 11:44
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('questions', '0021_auto_20180110_1750'),
    ]

    operations = [
        migrations.AddField(
            model_name='campaign',
            name='featured',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='campaign',
            name='public',
            field=models.BooleanField(default=False),
        ),
    ]
