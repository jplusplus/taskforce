# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-11-07 12:27
from __future__ import unicode_literals

import django.contrib.postgres.fields.jsonb
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('questions', '0007_auto_20171107_1226'),
    ]

    operations = [
        migrations.AlterField(
            model_name='campaign',
            name='intro_text',
            field=django.contrib.postgres.fields.jsonb.JSONField(default=b'[{"text": "Hi! Let\'s do this!", "type": "question", "id": "intro", "choices": [{"value": "start", "label": "Ready!"}]}]'),
        ),
    ]
