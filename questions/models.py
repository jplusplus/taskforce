# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import AbstractUser
from django.contrib.postgres.fields import JSONField
from questions.defaults import DEFAULT_INTRO_TEMPLATE, DEFAULT_QUESTION_TEMPLATE, DEFAULT_TUTORIAL
import json


def make_callable_data(data):
    # create new instances of the default data for JSONfields
    # see https://docs.djangoproject.com/en/1.10/ref/contrib/postgres/fields/#django.contrib.postgres.fields.JSONField
    if type(data) == list:
        return list(data)
    elif type(data) == dict:
        return dict(data)
    else:
        import copy
        return copy.deepcopy(data)


class User(AbstractUser):
    telegram_id = models.CharField(max_length=30, blank=True)


class UserStats(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    tutorial_done = models.BooleanField(default=False)
    total_points = models.PositiveIntegerField(null=True)


class Campaign(models.Model):
    TYPE_CHOICES = (
        ('image', 'Image'),
        ('text', 'Text'),
    )

    title = models.CharField(max_length=200)
    name = models.CharField(max_length=100, unique=True)
    tagline = models.CharField(max_length=500, blank=True)
    description = models.TextField(max_length=5000, blank=True)
    type = models.CharField(max_length=15, choices=TYPE_CHOICES, default='image')
    open = models.BooleanField(default=False)

    max_answers = models.PositiveIntegerField(default=2)
    owner = models.ForeignKey(User, related_name="owner", default=1, on_delete=models.SET_DEFAULT)
    users = models.ManyToManyField(User, through='CampaignStats')

    public = models.BooleanField(default=False)
    featured = models.BooleanField(default=False)

    intro_template = JSONField(default=make_callable_data(DEFAULT_INTRO_TEMPLATE))
    question_template = JSONField(default=make_callable_data(DEFAULT_QUESTION_TEMPLATE))
    tutorial = JSONField(default=make_callable_data(DEFAULT_TUTORIAL), null=True)
    tutorial_enabled = models.BooleanField(default=True)

    telegram_enabled = models.BooleanField(default=False)
    telegram_active = models.BooleanField(default=False)
    telegram_token = models.CharField(max_length=60, blank=True, null=True)
    telegram_botname = models.CharField(max_length=60, blank=True, null=True, editable=False)

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        # check if Telegram token has changed, and update bot name to reflect this
        # https://stackoverflow.com/a/45893548/122400
        import requests
        old = Campaign.objects.filter(pk=getattr(self, 'pk', None)).first()
        if old:
            # value changed
            if old.telegram_token != self.telegram_token:
                # remove bot
                # add bot
                response = requests.post('http://localhost:9500/remove', json={'campaign': self.name})
                response = requests.post('http://localhost:9500/add',
                                         json={'campaign': self.name, 'token': self.telegram_token})
                bot_info = json.loads(response.text)
                self.telegram_botname = bot_info['username']
                self.telegram_enabled = True
                self.telegram_active = True
        elif self.telegram_token:
            # new instance
            response = requests.post('http://localhost:9500/remove', json={'campaign': self.name})
            response = requests.post('http://localhost:9500/add',
                                     json={'campaign': self.name, 'token': self.telegram_token})
            bot_info = json.loads(response.text)
            self.telegram_botname = bot_info['username']
            self.telegram_enabled = True
            self.telegram_active = True
        super(Campaign, self).save(*args, **kwargs)


class CampaignStats(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    campaign = models.ForeignKey(Campaign, on_delete=models.CASCADE)
    points = models.PositiveIntegerField(default=0)


class Question(models.Model):
    campaign = models.ForeignKey(Campaign, related_name='questions', on_delete=models.CASCADE)
    human_id = models.CharField(max_length=400)
    text = models.CharField(max_length=500, blank=True)
    media_url = models.URLField(max_length=500, blank=True)
    extra = JSONField(blank=True, null=True)
    validated_value = models.CharField(max_length=200, blank=True)
    status = models.CharField(max_length=20, default="open")

    class Meta:
        unique_together = ('campaign', 'human_id')

    def update_status(self):
        if self.answers.count() < self.campaign.max_answers:
            self.status = "open"
        else:
            if len(set(self.answers.values_list('value', flat=True))) == 1:
                self.status = "closed-solved"
            else:
                self.status = "closed-conflict"
        self.save()

    def __str__(self):
        return self.human_id


class Answer(models.Model):
    user = models.ForeignKey(User, blank=True, null=True, on_delete=models.SET_NULL)
    question = models.ForeignKey(Question, related_name='answers', on_delete=models.SET_NULL, null=True)
    value = models.CharField(max_length=200)
    timestamp = models.DateTimeField(auto_now_add=True)
    points = models.PositiveIntegerField(default=5)

    def save(self, *args, **kwargs):
        super(Answer, self).save(*args, **kwargs)
        if self.user:
            cs, created = CampaignStats.objects.get_or_create(user=self.user, campaign=self.question.campaign)
            cs.points += self.points
            cs.save()
        self.question.update_status()
