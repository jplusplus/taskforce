# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import os
import shutil
import json
import zipfile
import unicodecsv as csv
from django.http import HttpResponse, JsonResponse
from django.shortcuts import render, redirect
from django.contrib import messages
from django.contrib.admin.views.decorators import staff_member_required
from djqscsv import render_to_csv_response
from tfcore import settings
from questions.models import Campaign, Question, Answer
from questions.forms import EditCampaignForm, NewCampaignForm, TelegramForm, ImageUploadForm, CSVUploadForm, EditTutorialForm, EditIntroForm, EditQuestionForm, SingleImageUploadForm

IMAGE_TYPES = ('.jpg', '.png', '.gif')


@staff_member_required
def index(request):
    context = {'campaigns': Campaign.objects.all()}
    return render(request, 'config/index.html', context)


@staff_member_required
def campaign_index(request, campaign_name):
    campaign = Campaign.objects.get(name=campaign_name)
    context = {'campaign': campaign,
               'all_questions': campaign.questions.all(),
               'solved_questions': campaign.questions.filter(status="closed-solved"),
               'conflict_questions': campaign.questions.filter(status="closed-conflict"),
               }
    return render(request, 'config/campaign-index.html', context)


def download_csv(request, campaign):
    c = Campaign.objects.get(name=campaign)
    questions = Question.objects.filter(campaign=c)
    answers = Answer.objects.filter(question__in=questions)
    return render_to_csv_response(answers)


@staff_member_required
def review(request, campaign_name):
    campaign = Campaign.objects.get(name=campaign_name)
    context = {'campaign': campaign,
               'conflict_questions': campaign.questions.filter(status="closed-conflict"),
               }
    return render(request, 'config/campaign-review.html', context)


@staff_member_required
def edit(request, campaign_name):
    campaign = Campaign.objects.get(name=campaign_name)
    if request.method == "POST":
        form = EditCampaignForm(request.POST, instance=campaign)
        if form.is_valid():
            campaign = form.save()
            messages.add_message(request, messages.SUCCESS, 'Campaign settings updated!')
            return redirect('config_campaign', campaign.name)
    else:
        form = EditCampaignForm(instance=campaign)
    context = {'campaign': campaign, 'form': form}
    return render(request, 'config/campaign-edit.html', context)


@staff_member_required
def new(request):
    if request.method == "POST":
        form = NewCampaignForm(request.POST)
        if form.is_valid():
            campaign = form.save()
            messages.add_message(request, messages.SUCCESS, 'New campaign created!')
            return redirect('config_campaign', campaign.name)
    else:
        form = NewCampaignForm()
    context = {'form': form}
    return render(request, 'config/campaign-new.html', context)


def handle_uploaded_file(f, campaign):
    # content-type: 'application/zip'
    uploads_dir = os.path.join(settings.UPLOADS_BASE_DIR, campaign.name + '/')
    if not os.path.exists(uploads_dir):
        os.makedirs(uploads_dir)
    question_count = 0
    with zipfile.ZipFile(f, 'r') as z:
        for filename in z.namelist():
            fn, ext = os.path.splitext(filename)
            if ext.lower() in IMAGE_TYPES:
                destination = os.path.join(uploads_dir, os.path.basename(filename))
                url = settings.MEDIA_URL + campaign.name + '/' + os.path.basename(filename)
                with z.open(filename) as zf, \
                        open(destination, 'wb') as nf:
                    shutil.copyfileobj(zf, nf)
                    Question.objects.create(
                        campaign=campaign,
                        human_id=os.path.basename(filename),
                        media_url=url
                    )
                question_count += 1
    return question_count


def handle_uploaded_csv(f, campaign):
    # content-type: 'application/csv'
    question_count = 0
    reader = csv.DictReader(f)
    for row in reader:
        print(row.get('id'))
        question = Question.objects.create(
            campaign=campaign,
            human_id=row.pop('id'),
        )
        if row.get('text'):
            question.text = row.pop('text')
        if row.get('media_url'):
            question.media_url = row.pop('media_url')
        # add other keys to the extra field
        if row:
            question.extra = {}
            for key in row:
                question.extra[key] = row[key]
        question.save()
        question_count += 1
    return question_count


@staff_member_required
def upload(request, campaign_name):
    campaign = Campaign.objects.get(name=campaign_name)
    if campaign.type == "image":
        form_class = ImageUploadForm
        handle_fn = handle_uploaded_file
    elif campaign.type == "text":
        form_class = CSVUploadForm
        handle_fn = handle_uploaded_csv
    if request.method == "POST":
        form = form_class(request.POST, request.FILES)
        if form.is_valid():
            question_count = handle_fn(request.FILES['file'], campaign)
            messages.add_message(request, messages.SUCCESS, '{} new questions successfully imported!'.format(question_count))
            return redirect('config_campaign', campaign.name)
    else:
        form = form_class()
    context = {'campaign': campaign, 'form': form}
    return render(request, 'config/campaign-upload.html', context)


@staff_member_required
def intro(request, campaign_name):
    campaign = Campaign.objects.get(name=campaign_name)
    if request.method == "POST":
        form = EditIntroForm(request.POST, instance=campaign)
        if form.is_valid():
            campaign = form.save()
            messages.add_message(request, messages.SUCCESS, 'Intro updated!')
            return redirect('config_campaign', campaign.name)
    else:
        form = EditIntroForm(instance=campaign)
    context = {'campaign': campaign, 'form': form}
    return render(request, 'config/campaign-intro.html', context)


@staff_member_required
def question(request, campaign_name):
    campaign = Campaign.objects.get(name=campaign_name)
    if request.method == "POST":
        form = EditQuestionForm(request.POST, instance=campaign)
        if form.is_valid():
            campaign = form.save()
            messages.add_message(request, messages.SUCCESS, 'Question updated!')
            return redirect('config_campaign', campaign.name)
    else:
        form = EditQuestionForm(instance=campaign)
    context = {'campaign': campaign, 'form': form}
    return render(request, 'config/campaign-question.html', context)


# --- Tutorial editor ---

@staff_member_required
def tutorial(request, campaign_name):
    campaign = Campaign.objects.get(name=campaign_name)
    if request.method == "POST":
        form = EditTutorialForm(request.POST, instance=campaign)
        if form.is_valid():
            campaign = form.save()
            messages.add_message(request, messages.SUCCESS, 'Tutorial updated!')
            return redirect('config_campaign', campaign.name)
    else:
        form = EditTutorialForm(instance=campaign)
    imageform = SingleImageUploadForm()
    context = {'campaign': campaign, 'form': form, 'imageform': imageform}
    return render(request, 'config/campaign-tutorial.html', context)


def handle_image_upload(request):
    if request.method == 'POST':
        form = SingleImageUploadForm(data=request.POST, files=request.FILES)
        if form.is_valid():
            # handle request.FILES
            f = request.FILES['file_source']
            destpath = os.path.join(settings.MEDIA_ROOT, f.name)
            with open(destpath, 'wb+') as destination:
                for chunk in f.chunks():
                    destination.write(chunk)
            return JsonResponse({
                'status': 'success',
                'url': settings.SITE_ROOT + settings.MEDIA_URL + f.name
            })
        else:
            return JsonResponse({
                'status': 'error',
                'errors': str(form.errors)
            })
    return JsonResponse({
        'status': 'error',
        'errors': 'Endpoint only accessible via POST'
    })


# --- Telegram ---

@staff_member_required
def telegram(request, campaign_name):
    campaign = Campaign.objects.get(name=campaign_name)
    if request.method == "post":
        form = TelegramForm(request.post, instance=campaign)
        if form.is_valid():
            campaign = form.save()
            messages.add_message(request, messages.SUCCESS, 'Telegram integration updated.')
            return redirect('config_campaign', campaign.name)
    else:
        form = TelegramForm(instance=campaign)
    context = {'campaign': campaign, 'form': form}
    return render(request, 'config/campaign-telegram.html', context)


def telegram_do_start(request, campaign_name):
    requests.get('http://localhost:9500/start/' + campaign_name)
    campaign.telegram_active = True
    campaign.save()
    return telegram(request, campaign_name)


def telegram_do_stop(request, campaign_name):
    requests.get('http://localhost:9500/stop/' + campaign_name)
    campaign.telegram_active = False
    campaign.save()
    return telegram(request, campaign_name)


def telegram_do_add(request, campaign_name):
    campaign = Campaign.objects.get(name=campaign_name)
    response = requests.post('http://localhost:9500/add',
                             json={'campaign': campaign.name, 'token': campaign.telegram_token})
    bot_info = json.loads(response.text)
    campaign.telegram_botname = bot_info['username']
    campaign.telegram_enabled = True
    campaign.telegram_active = True
    campaign.save()
    return telegram(request, campaign_name)


def telegram_do_remove(request, campaign_name):
    requests.post('http://localhost:9500/remove', json={'campaign': campaign.name})
    campaign.telegram_botname = None
    campaign.telegram_enabled = False
    campaign.telegram_active = False
    campaign.save()
    return telegram(request, campaign_name)


@staff_member_required
def participants(request, campaign_name):
    campaign = Campaign.objects.get(name=campaign_name)
    context = {'campaign': campaign,
               'leaderboard': campaign.campaignstats_set.order_by('-points'),
               'users': campaign.users.all(),
               'answers': Answer.objects.all()
               }
    return render(request, 'config/campaign-participants.html', context)


def do_validate_question(request, campaign, qid, value):
    c = Campaign.objects.get(name=campaign)
    q = Question.objects.get(campaign=c, human_id=qid)
    q.validated_value = value
    q.save()
    return JsonResponse({'status': 'ok'})


def get_intro_template(request, campaign):
    c = Campaign.objects.get(name=campaign)
    return JsonResponse(c.intro_template, safe=False)


def set_intro_template(request, campaign):
    intro_data = request.POST.get('intro_data')
    data = json.loads(intro_data)
    c = Campaign.objects.get(name=campaign)
    c.intro_template = data
    c.save()
    return JsonResponse({'status': 'ok'})


def get_question_template(request, campaign):
    c = Campaign.objects.get(name=campaign)
    return JsonResponse(c.question_template, safe=False)


def set_question_template(request, campaign):
    question_data = request.POST.get('question_data')
    data = json.loads(question_data)
    c = Campaign.objects.get(name=campaign)
    c.question_template = data
    c.save()
    return JsonResponse({'status': 'ok'})


def get_tutorial(request, campaign):
    c = Campaign.objects.get(name=campaign)
    return JsonResponse(c.tutorial, safe=False)


def set_tutorial(request, campaign):
    tutorial_data = request.POST.get('tutorial_data')
    data = json.loads(tutorial_data)
    c = Campaign.objects.get(name=campaign)
    c.tutorial = data
    c.save()
    return JsonResponse({'status': 'ok'})
