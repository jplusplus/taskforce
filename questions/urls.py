from django.conf.urls import url

from . import views, views_chat, views_config

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^campaign$', views.campaign_list, name='campaign_list'),
    url(r'^campaign/(?P<campaign_name>[a-z-]+)$', views.campaign_detail, name='campaign_detail'),

    # Private endpoints
    url(r'^private/get-campaigns/$', views.get_telegram_campaigns, name='get_telegram_campaigns'),
    url(r'^private/get-telegram-user/$', views.get_telegram_user, name='get_telegram_user'),
    url(r'^private/register-telegram-user/$', views.register_telegram_user, name='register_telegram_user'),

    # Campaign config
    url(r'^config/$', views_config.index, name='config_index'),
    url(r'^config/new$', views_config.new, name='config_new'),
    url(r'^config/upload-image$', views_config.handle_image_upload, name='upload_image'),
    url(r'^(?P<campaign_name>[a-z-]+)/config/$', views_config.campaign_index, name='config_campaign'),
    url(r'^(?P<campaign_name>[a-z-]+)/config/edit$', views_config.edit, name='config_edit'),
    url(r'^(?P<campaign_name>[a-z-]+)/config/upload$', views_config.upload, name='config_upload'),
    url(r'^(?P<campaign_name>[a-z-]+)/config/review$', views_config.review, name='config_review'),
    url(r'^(?P<campaign_name>[a-z-]+)/config/participants$', views_config.participants, name='config_participants'),
    url(r'^(?P<campaign_name>[a-z-]+)/config/tutorial$', views_config.tutorial, name='config_tutorial'),
    url(r'^(?P<campaign_name>[a-z-]+)/config/intro$', views_config.intro, name='config_intro'),
    url(r'^(?P<campaign_name>[a-z-]+)/config/question$', views_config.question, name='config_question'),
    url(r'^(?P<campaign_name>[a-z-]+)/config/telegram$', views_config.telegram, name='config_telegram'),
    url(r'^(?P<campaign_name>[a-z-]+)/config/telegram/start$', views_config.telegram_do_start, name='config_telegram_start'),
    url(r'^(?P<campaign_name>[a-z-]+)/config/telegram/stop$', views_config.telegram_do_start, name='config_telegram_stop'),
    url(r'^(?P<campaign_name>[a-z-]+)/config/telegram/add$', views_config.telegram_do_add, name='config_telegram_add'),
    url(r'^(?P<campaign_name>[a-z-]+)/config/telegram/remove$', views_config.telegram_do_remove, name='config_telegram_remove'),

    url(r'^(?P<campaign>[a-z-]+)/config/get-intro/$', views_config.get_intro_template, name='get_intro_template'),
    url(r'^(?P<campaign>[a-z-]+)/config/set-intro/$', views_config.set_intro_template, name='set_intro_template'),
    url(r'^(?P<campaign>[a-z-]+)/config/get-question/$', views_config.get_question_template, name='get_question_template'),
    url(r'^(?P<campaign>[a-z-]+)/config/set-question/$', views_config.set_question_template, name='set_question_template'),
    url(r'^(?P<campaign>[a-z-]+)/config/get-tutorial/$', views_config.get_tutorial, name='get_tutorial_template'),
    url(r'^(?P<campaign>[a-z-]+)/config/set-tutorial/$', views_config.set_tutorial, name='set_tutorial_template'),
    url(r'^(?P<campaign>[a-z-]+)/config/csv/$', views_config.download_csv, name='download_csv'),

    # Campaign endpoints
    url(r'^(?P<campaign>[a-z-]+)/get-intro/$', views_chat.get_intro, name='get_intro'),
    url(r'^(?P<campaign>[a-z-]+)/get-tutorial/$', views_chat.get_tutorial, name='get_tutorial'),
    url(r'^(?P<campaign>[a-z-]+)/q/$', views_chat.get_question, name='get_random_question'),
    url(r'^(?P<campaign>[a-z-]+)/q/(?P<question_id>[0-9a-z-]+)/$', views_chat.get_question, name='get_question'),
    url(r'^(?P<campaign>[a-z-]+)/a/$', views_chat.receive_answer, name='receive_answer'),
    url(r'^(?P<campaign>[a-z-]+)/text/$', views_chat.receive_text, name='receive_text'),

    # Browser chat interface
    url(r'^(?P<campaign>[a-z-]+)/$', views.chat, name='chat'),


]
