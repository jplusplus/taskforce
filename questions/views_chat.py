from django.contrib.auth import authenticate, login, logout
from django.template.context_processors import csrf
from django.db.models import Count
from django.http import JsonResponse
from questions.models import Campaign, Question, Answer, User
from questions.utils import render_template_tags, hash_password, check_password, text_bit
import json


def get_question(request, campaign, question_id=None, prepend_bits=None, extra=None, inner_extra=None):
    c = Campaign.objects.get(name=campaign)
    if not question_id:
        qs = Question.objects.filter(campaign=c).annotate(answer_count=Count('answers')).filter(answer_count__lt=c.max_answers)
        if not qs:
            return simple_text_message("This campaign is complete! There are no more questions to be answered. Why don't you try another?")

        if request.user.is_authenticated:
            qs = qs.exclude(answers__user=request.user)
        if not qs:
            return simple_text_message("Wow, you've gone through all the available questions! Thank you so much! Now why don't you try another campaign?")
        import random
        q = random.choice(qs)
    else:
        q = Question.objects.get(campaign=c, human_id=question_id)
    reply = {}
    template = c.question_template
    # add question ID to the question
    template[-1]["id"] = q.human_id
    reply['bits'] = json.loads(render_template_tags(json.dumps(c.question_template), q))
    if extra:
        reply['extra_fields'] = extra
    if inner_extra:
        reply.update(inner_extra)
    if prepend_bits:
        prepend_bits.reverse()
        for bit in prepend_bits:
            reply['bits'].insert(0, bit)
    return JsonResponse(reply)


def get_tutorial(request, campaign, prepend_bits=None, extra=None, inner_extra=None):
    c = Campaign.objects.get(name=campaign)
    tutorial_bits = list(c.tutorial)
    if prepend_bits:
        prepend_bits.reverse()
        for b in prepend_bits:
            tutorial_bits.insert(0, b)
    return get_question(request, campaign, prepend_bits=tutorial_bits, extra=extra, inner_extra=inner_extra)


def receive_text(request, campaign):
    message = request.POST.get('message').strip()
    msgid = request.POST.get('msgid')
    qid = request.POST.get('questionid')
    state = request.POST.get('state')

    if message == "login" or (state and state.startswith("login")):
        return do_login(request, campaign)
    elif message == "logout":
        return do_logout(request, campaign)
    elif message == "ERROR":
        # testing purposes!
        assert False
    elif message == "register" or (state and state.startswith("register")):
        return do_register(request, campaign)
    elif qid:
        q = Question.objects.get(human_id=qid)
        a = Answer.objects.create(question=q, value=message)
        a.save()
        return get_question(request, campaign=campaign)
    else:
        adjectives = ['smart', 'clever', 'insightful', 'wise', 'sensible', 'eloquent']
        from random import choice
        msg = 'You typed "%s"! So %s! Now can you please answer my previous question?' % (message, choice(adjectives))
        reply = {'replying_to': msgid,
                 'bits': [{'type': 'text', 'text': msg}]}
        return JsonResponse(reply)


def receive_answer(request, campaign):
    question_id = request.POST.get('question_id')
    value = request.POST.get('answer_value')
    c = Campaign.objects.get(name=campaign)

    if value and value == ':login:':
        return do_login(request, campaign)
    elif value and value == ':register:':
        return do_register(request, campaign)
    if value and not (question_id == "intro" and value == "start"):
        user = None
        if request.user.is_authenticated:
            user = request.user
        q = Question.objects.get(campaign=c, human_id=question_id)
        a = Answer.objects.create(question=q, value=value, user=user)
        a.save()
    return get_question(request, campaign)


def get_intro(request, campaign):
    # FIXME: security hole
    if request.GET.get('telegram_id'):
        users = User.objects.filter(telegram_id=request.GET.get('telegram_id'))
        if users:
            u = users[0]
            login(request, u)
    c = Campaign.objects.get(name=campaign)
    if request.user.is_authenticated:
        bits = [text_bit("Welcome back, %s!" % request.user.username),
                text_bit("Let's get back to the questions!")]
        return get_question(request, campaign,
                            prepend_bits=bits)
    else:
        reply = {}
        bits = [text_bit("Before starting, we need to log you in."),
                text_bit("If this is your first time here, we just need to quickly sign you up."),
                {"type": "question",
                 "id": "auth",
                 "text": "Otherwise, just log in.",
                 "choices": [
                     {"value": ":login:",
                      "label": "Log me in"
                      },
                     {"value": ":register:",
                      "label": "I'm new, sign me up"
                      }]
                 }]

        reply['bits'] = c.intro_template
        reply['bits'].extend(bits)
        return JsonResponse(reply)


def simple_text_message(msg, extra=None, inner_extra=None, expects=None):
    reply = {'bits': [{'type': 'text', 'text': msg}]}
    if expects:
        reply['bits'][0]['expects'] = 'text'
    if extra:
        reply['extra_fields'] = extra
    if inner_extra:
        reply.update(inner_extra)
    return JsonResponse(reply)


def simple_text_question(msg, extra=None, inner_extra=None):
    reply = {'bits': [{'type': 'question', 'text': msg, 'expects': 'text'}]}
    if extra:
        reply['extra_fields'] = extra
    if inner_extra:
        reply.update(inner_extra)
    return JsonResponse(reply)


def do_login(request, campaign):
    message = request.POST.get('message', '').strip()
    state = request.POST.get('state')
    if not state or (state and not state.startswith("login")):
        if request.user.is_authenticated:
            return simple_text_message("You're already logged in as %s!" % request.user.username)
        return simple_text_message("What's your username?",
                                   extra={'state': 'login-get-username'},
                                   expects="text")
    elif state == "login-get-username":
        username = message
        return simple_text_message("What's your password?", expects="text",
                                   extra={'state': 'login-get-password', 'username': username})
    elif state == "login-get-password":
        username = request.POST.get('username')
        password = message
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                # https://stackoverflow.com/a/33028531/122400
                from django.template.context_processors import csrf
                return get_question(request, campaign,
                                    prepend_bits=[text_bit("Logged in! Let's start!")],
                                    inner_extra={'csrf_token': str(csrf(request)['csrf_token'])}
                                    )
        else:
            return simple_text_message("Login failed :( Try again by typing 'login'")
    else:
        assert False


def do_logout(request, campaign):
    if not request.user.is_authenticated:
        return simple_text_message("You're not logged in.")
    logout(request)
    return simple_text_message("You're now logged out.")


def do_register(request, campaign):
    message = request.POST.get('message', '').strip()
    state = request.POST.get('state')
    if not state or (state and not state.startswith("register")):
        return simple_text_question("What's your desired username?",
                                    extra={'state': 'register-set-username'})
    elif state == "register-set-username":
        username = message
        if User.objects.filter(username=username):
            return simple_text_question("That username already exists! Tell me another username for you.",
                                        extra={'state': 'register-set-username'})
        else:
            return simple_text_question("What's your email address?",
                                        extra={'state': 'register-set-email',
                                               'username': username})
    elif state == "register-set-email":
        email = message
        username = request.POST.get('username')
        return simple_text_question("What's your desired password?",
                                    extra={'state': 'register-set-password',
                                           'username': username,
                                           'email': email})
    elif state == "register-set-password":
        password_hash = hash_password(message)
        username = request.POST.get('username')
        email = request.POST.get('email')
        return simple_text_message("Type your password again for confirmation.", expects="text",
                                   extra={'state': 'register-confirm-password',
                                          'username': username,
                                          'email': email,
                                          'password_hash': password_hash})
    elif state == "register-confirm-password":
        password = message
        username = request.POST.get('username')
        email = request.POST.get('email')
        password_hash = request.POST.get('password_hash')
        if not check_password(password_hash, password):
            # passwords didn't match
            return simple_text_message("Passwords didn't match! Let's try this again. What's your desired password?",
                                       extra={'state': 'register-set-password',
                                              'username': username,
                                              'email': email})
        # https://docs.djangoproject.com/en/1.11/topics/auth/default/#creating-users
        user = User.objects.create_user(username, email, password)
        login(request, user)
        c = Campaign.objects.get(name=campaign)
        print(c)
        if c.tutorial_enabled and c.tutorial:
            return get_tutorial(request, campaign,
                                prepend_bits=[text_bit("Registered and logged in! Welcome, %s!" % username),
                                              text_bit("Now, let's do a quick tutorial!")],
                                inner_extra={'csrf_token': str(csrf(request)['csrf_token'])}
                                )
        else:
            return get_question(request, campaign,
                                prepend_bits=[text_bit("Registered and logged in! Welcome, %s!" % username),
                                              text_bit("Now, let's start!")],
                                inner_extra={'csrf_token': str(csrf(request)['csrf_token'])}
                                )
