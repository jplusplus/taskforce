# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from .models import Question, Campaign, Answer, User

admin.site.register(Campaign)
admin.site.register(Question)
admin.site.register(Answer)
admin.site.register(User)

admin.site.site_header = 'Taskforce Editor login'
admin.site.site_title = 'Taskforce Editor area'
