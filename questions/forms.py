from django import forms
from django.core.validators import FileExtensionValidator
from questions.models import Campaign


class IntroEditor(forms.Widget):
    template_name = 'config/widget-intro.html'


class QuestionEditor(forms.Widget):
    template_name = 'config/widget-question.html'


class TutorialEditor(forms.Widget):
    template_name = 'config/widget-tutorial.html'


class NewCampaignForm(forms.ModelForm):
    class Meta:
        model = Campaign
        exclude = ['open', 'users', 'owner', 'public', 'featured',
                   'intro_template', 'question_template', 'tutorial', 'tutorial_enabled',
                   'telegram_enabled',
                   'telegram_active',
                   'telegram_token']
        help_texts = {
            'title': 'The publicly shown title of your campaign.',
            'name': 'A lowercase version of the campaign title to be used in URLs, e.g. "https://taskforce.jplusplus.org/campaign-name". Use only lowercase, numbers and the hyphen character.',
            'tagline': 'A one-line description of what your campaign is about and its goal.',
            'description': 'A more detailed explanation of your campaign to be featured in its public page. Feel free to include all details and relevant links. Markdown is supported.',
            'type': 'Select "Image" if your campaign is based on pictures, and "Text" if you only have text-based questions. NOTE: This setting cannot be changed later.',
            'max_answers': 'The number of answers at which the question will be considered solved and won\'t be presented to more people. Your campaign will take longer the higher this number is, but you can be assured of better accuracy. If you\'re unsure, 3 is a good number for this setting.',
        }


class EditCampaignForm(forms.ModelForm):
    class Meta:
        model = Campaign
        exclude = ['name', 'users', 'owner', 'type', 'public', 'featured',
                   'tutorial_enabled', 'tutorial',
                   'intro_template', 'question_template',
                   'telegram_enabled', 'telegram_active', 'telegram_token'
                   ]
        help_texts = {
            'title': 'The publicly shown title of your campaign.',
            'tagline': 'A one-line description of what your campaign is about and its goal.',
            'description': 'A more detailed explanation of your campaign to be featured in its public page. Feel free to include all details and relevant links. Markdown is supported.',
            'open': 'If deactivated, the campaign won\'t be accessible to users. Use this setting if you need to suspend your campaign for some reason.',
            'max_answers': 'The number of answers at which the question will be considered solved and won\'t be presented to more people. Your campaign will take longer the higher this number is, but you can be assured of better accuracy. If you\'re unsure, 3 is a good number for this setting.',
        }


class EditTutorialForm(forms.ModelForm):
    class Meta:
        model = Campaign
        fields = ['tutorial_enabled', 'tutorial']
        widgets = {'tutorial': TutorialEditor}


class EditIntroForm(forms.ModelForm):
    class Meta:
        model = Campaign
        fields = ['intro_template']
        widgets = {'intro_template': IntroEditor}


class EditQuestionForm(forms.ModelForm):
    class Meta:
        model = Campaign
        fields = ['question_template']
        widgets = {'question_template': QuestionEditor}


class ImageUploadForm(forms.Form):
    file = forms.FileField(validators=[FileExtensionValidator(['zip'])])


class CSVUploadForm(forms.Form):
    file = forms.FileField(validators=[FileExtensionValidator(['csv'])])


class SingleImageUploadForm(forms.Form):
    file_source = forms.FileField()


class TelegramForm(forms.ModelForm):
    class Meta:
        model = Campaign
        fields = ['telegram_enabled',
                  'telegram_active',
                  'telegram_token']
