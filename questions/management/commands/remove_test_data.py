#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand
from questions.models import User, Campaign, Answer
from tfcore import settings
import datetime


class Command(BaseCommand):
    help = 'Populates the database with the test campaigns.'

    def _remove_data(self):
        usernames = ['chinfrins', 'muletas', 'gatoassanhado', 'chamuças', 'grebos']
        for username in usernames:
            if User.objects.filter(username=username):
                u = User.objects.get(username=username)
                if u.answer_set.all():
                    for a in u.answer_set.all():
                        a.delete()

    def handle(self, *args, **options):
        self._remove_data()
