#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand
from questions.models import User, Campaign, Answer
from tfcore import settings
import datetime


class Command(BaseCommand):
    help = 'Populates the database with the test campaigns.'

    usernames = ['chinfrins', 'muletas', 'gatoassanhado', 'chamuças', 'grebos']
    campaign_name = "genderfinder"

    def _insert_data(self):
        # create mock users
        for username in self.usernames:
            if User.objects.filter(username=username):
                u = User.objects.get(username=username)
                if u.answer_set.all():
                    for a in u.answer_set.all():
                        a.delete()
            else:
                u = User.objects.create_user(username, password='taskforce')
                u.save()
        # for each question in Genderfinder (except 3), insert answers by 2 users
        c = Campaign.objects.get(name=self.campaign_name)
        questions = list(c.questions.order_by('?'))
        questions.pop()
        questions.pop()
        questions.pop()
        import random
        for q in questions:
            # get 2 random users
            users = list(self.usernames)
            random.shuffle(users)
            answers = ('m', 'f')
            for username in users[:c.max_answers]:
                a = Answer.objects.create(user=User.objects.get(username=username),
                                          question=q,
                                          value=random.choice(answers),
                                          timestamp=datetime.datetime.now())

    def handle(self, *args, **options):
        self._insert_data()
