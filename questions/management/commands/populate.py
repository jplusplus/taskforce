#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import glob
import json
import csv
from django.core.management.base import BaseCommand
from questions.models import Campaign, Question, User
from tfcore import settings


class Command(BaseCommand):
    help = 'Populates the database with the test campaigns.'

    def _insert_data(self):
        u = User.objects.create_user(username='rlafuente', password='taskforce')
        u.is_superuser = True
        u.is_staff = True
        u.save()
        u = User.objects.create_user(username='dni', password='dnireview')
        u.is_superuser = True
        u.is_staff = True
        u.save()
        for f in glob.glob(settings.FIXTURES_DIR + "campaign-*.json"):
            # each json file contains campaign data
            cdata = json.loads(open(f, 'r').read())
            campaign = Campaign.objects.create(
                open=True,
                title=cdata['title'],
                name=cdata['name'],
                type=cdata['type'],
                tagline=cdata.get('tagline', ''),
                description=open('fixtures/' + cdata.get('description_file', 'desc-default.md'), 'r').read(),
                question_template=cdata['question_template'],
                intro_template=cdata['intro_template'],
                public=cdata['public'],
                featured=cdata['featured'],

                telegram_enabled=bool(cdata.get('telegram_enabled')),
                telegram_active=bool(cdata.get('telegram_active')),
                telegram_token=cdata.get('telegram_token'),
            )
            if cdata.get('tutorial'):
                campaign.tutorial = cdata['tutorial']
                campaign.tutorial_enabled = True
            campaign.save()
            print("Importing %s..." % cdata['title'])
            urls_file = os.path.join(settings.FIXTURES_DIR, cdata['data_file'])
            with open(urls_file) as f:
                reader = csv.DictReader(f)
                for row in reader:
                    question = Question.objects.create(
                        campaign=campaign,
                        human_id=row.pop('id'),
                    )
                    if row.get('text'):
                        question.text = row.pop('text')
                    if row.get('media_url'):
                        question.media_url = row.pop('media_url')
                    # add other keys to the extra field
                    if row:
                        question.extra = {}
                        for key in row:
                            question.extra[key] = row[key]
                    question.save()

    def handle(self, *args, **options):
        self._insert_data()
