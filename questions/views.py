# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.middleware.csrf import get_token
from django.contrib.auth import login
import json
from questions.models import Campaign, User, Answer


def index(request):
    context = {'featured_campaigns': Campaign.objects.filter(featured=True)}
    return render(request, 'index.html', context)


def campaign_list(request):
    context = {'public_campaigns': Campaign.objects.filter(public=True)}
    return render(request, 'campaign_list.html', context)


def campaign_detail(request, campaign_name):
    campaign = Campaign.objects.get(name=campaign_name)
    context = {'campaign': campaign,
               'leaderboard': campaign.campaignstats_set.order_by('-points'),
               'answers': Answer.objects.filter(question__campaign=campaign)
               }
    return render(request, 'campaign_detail.html', context)


def chat(request, campaign):
    c = Campaign.objects.get(name=campaign)
    context = {'campaign': c}
    return render(request, 'chat.html', context)


def get_telegram_campaigns(request):
    results = []
    for campaign in Campaign.objects.filter(open=True, telegram_active=True, telegram_enabled=True):
        c = {'campaign': campaign.name,
             'username': campaign.telegram_botname,
             'token': campaign.telegram_token}
        results.append(c)
    return JsonResponse(results, safe=False)


@csrf_exempt
def get_telegram_user(request):
    if not request.POST.get('id'):
        b = request.body.decode('utf-8')
        print(b)
        post = json.loads(b)
    else:
        post = request.POST
    user_id = post.get('id')
    users = User.objects.filter(telegram_id=user_id)
    results = {}
    if users:
        u = users[0]
        login(request, u)
        results['found'] = True
        results['username'] = u.username
        results['id'] = u.telegram_id
        from django.template.context_processors import csrf
        results['csrf_token'] = str(csrf(request)['csrf_token'])
    else:
        results['found'] = False
    return JsonResponse(results, safe=False)


@csrf_exempt
def register_telegram_user(request):
    if not request.POST.get('id'):
        post = json.loads(request.body)
    else:
        post = request.POST
    user_id = post.get('id')
    username = post.get('username')
    password = post.get('password')
    user = User.objects.create_user(username=username, email=None, password=password, telegram_id=user_id)
    results = {'created': True}
    return JsonResponse(results, safe=False)

