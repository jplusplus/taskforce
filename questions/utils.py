from django.template import engines
import uuid
import hashlib


def render_template_tags(text, question):
    '''Transforms the {{}} tags into the corresponding question fields.'''
    engine = engines['jinja2']
    template = engine.from_string(text)
    context = {'question': question}
    context['text'] = question.text
    context['media_url'] = question.media_url
    if question.extra:
        for field in question.extra:
            context[field] = question.extra[field]
    return template.render(context=context)


# password hash functions
# source: http://pythoncentral.io/hashing-strings-with-python/
def hash_password(password):
    # uuid is used to generate a random number
    salt = uuid.uuid4().hex
    return hashlib.sha256(salt.encode() + password.encode()).hexdigest() + ':' + salt


def check_password(hashed_password, user_password):
    password, salt = hashed_password.split(':')
    return password == hashlib.sha256(salt.encode() + user_password.encode()).hexdigest()


def text_bit(txt):
    return {"type": "text", "text": txt}
