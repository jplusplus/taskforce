DEFAULT_INTRO_TEMPLATE = [
    {'type': 'question',
        'id': 'intro',
        'text': '',
        'choices': [
            {'value': 'start',
             'label': 'Ready!'}]
     }
]

DEFAULT_QUESTION_TEMPLATE = [{
    "type": "question-image",
    "media_url": "{{ question.media_url }}",
    "text": "{{ question.text }}",
    "choices": [
        {"value": "y",
         "label": "Yes"},
        {"value": "n",
         "label": "No"},
        {"value": "x",
         "label": "Skip"}
    ]
}]

DEFAULT_TUTORIAL = [
    {
        "type": "text",
        "text": "This is a dummy tutorial."
    },
    {
        "type": "question-tutorial",
        "text": "Testing questions, what is the right answer?",
        "choices": [
            {
                "label": "Right",
                "next": {
                    "type": "text",
                    "text": "Correct! This was the right answer!"
                }
            },
            {
                "label": "Wrong",
                "next": {
                    "type": "text",
                    "text": "Uh no, this was the wrong answer."
                }
            }
        ],
    },
    {
        "type": "text",
        "text": "Tutorial over!"
    },
]
