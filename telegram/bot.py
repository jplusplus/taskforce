#!/usr/bin/env python3

# import os
import asyncio
from telepot import message_identifier, glance
import telepot
import telepot.aio
import telepot.aio.helper
from telepot.namedtuple import InlineKeyboardMarkup, InlineKeyboardButton
from telepot.aio.delegate import per_chat_id, create_open, pave_event_space, include_callback_query_chat_id
from telepot.aio.loop import OrderedWebhook
from aiohttp import web
import requests
import json
import emoji

BASE_URL = "https://taskforce.jplusplus.org/"

DATA_SEPARATOR = '|'

ENDPOINT_MESSAGE = "text"
ENDPOINT_START = "get-intro"
ENDPOINT_TUTORIAL = "get-tutorial"
ENDPOINT_QUESTION = "q"
ENDPOINT_ANSWER = "a"


def build_inline_keyboard(qid, choices):
    buttons = []
    for c in choices:
        # label = emoji.emojize(c['label'], use_aliases=True)
        callback_data = str(qid) + DATA_SEPARATOR + str(c.get('value', c['label'].lower()))
        buttons.append(InlineKeyboardButton(text=c['label'], callback_data=callback_data))
    return InlineKeyboardMarkup(inline_keyboard=[buttons])


class TaskforceBot(telepot.aio.helper.ChatHandler):
    def __init__(self, *args, **kwargs):
        super(TaskforceBot, self).__init__(*args, **kwargs)
        self._count = 0
        self._edit_msg_ident = None
        self._editor = None
        self.started = False
        self.expecting_answer_to = None

        self.remaining_tutorial_bits = []

        bot = args[0][0]
        self.campaign = bot.campaign
        api_base_url = BASE_URL + self.campaign + "/"

        self.session = requests.session()
        response = self.session.get(api_base_url)
        self.csrftoken = self.session.cookies['csrftoken']
        self.api_urls = {
            'message': api_base_url + ENDPOINT_MESSAGE + "/",
            'start': api_base_url + ENDPOINT_START + "/",
            'tutorial': api_base_url + ENDPOINT_TUTORIAL + "/",
            'get-question': api_base_url + ENDPOINT_QUESTION + "/",
            'answer': api_base_url + ENDPOINT_ANSWER + "/",
        }

    async def _process_response_bits(self, bits):
        for bit in bits:
            if bit['type'] == 'text':
                text = emoji.emojize(bit['text'], use_aliases=True)
                await self.sender.sendMessage(text, parse_mode="Markdown")

            elif bit['type'] == 'image':
                await self.sender.sendChatAction(action='upload_photo')
                img_url = bit['media_url']
                await self.sender.sendPhoto(img_url, caption=bit['text'])

            elif bit['type'] == 'question':
                text = emoji.emojize(bit['text'], use_aliases=True)
                keyboard = None
                if bit.get('choices'):
                    keyboard = build_inline_keyboard(bit['id'], bit['choices'])
                if bit.get('expects'):
                    self.expecting_answer_to = bit['id']
                sent = await self.sender.sendMessage(text, parse_mode="Markdown", reply_markup=keyboard)
                self._editor = telepot.aio.helper.Editor(self.bot, sent)
                self._edit_msg_ident = message_identifier(sent)
                if not bit.get('choices') and not bit.get('expects'):
                    raise ValueError('Malformed question!')

            elif bit['type'] == 'question-tutorial':
                # remove all previous bits from tutorial list
                bit_idx = self.remaining_tutorial_bits.index(bit)
                self.remaining_tutorial_bits = self.remaining_tutorial_bits[bit_idx:]
                bit['id'] = 'tutorial'
                text = emoji.emojize(bit['text'], use_aliases=True)
                keyboard = None
                if bit.get('choices'):
                    keyboard = build_inline_keyboard('tutorial', bit['choices'])
                if bit.get('expects'):
                    self.expecting_answer_to = bit['id']
                if bit.get('media_url'):
                    await self.sender.sendChatAction(action='upload_photo')
                    sent = await self.sender.sendPhoto(bit['media_url'], caption=text, reply_markup=keyboard)
                else:
                    sent = await self.sender.sendMessage(text, parse_mode="Markdown", reply_markup=keyboard)
                self._editor = telepot.aio.helper.Editor(self.bot, sent)
                self._edit_msg_ident = message_identifier(sent)
                # don't read further, when we get the "tutorial" callback it resumes the tutorial
                break

            elif bit['type'] == 'question-image':
                await self.sender.sendChatAction(action='upload_photo')
                caption = emoji.emojize(bit['text'], use_aliases=True)
                img_url = bit['media_url']
                keyboard = None
                if bit.get('choices'):
                    keyboard = build_inline_keyboard(bit['id'], bit['choices'])
                if bit.get('expects'):
                    self.expecting_answer_to = bit['id']
                sent = await self.sender.sendPhoto(img_url, caption=caption, reply_markup=keyboard)
                self._editor = telepot.aio.helper.Editor(self.bot, sent)
                self._edit_msg_ident = message_identifier(sent)
                if not bit.get('choices') and not bit.get('expects'):
                    raise ValueError('Malformed question!')
            # slight delay between responses
            from time import sleep
            sleep(0.75)

    def _send_answer(self, qid, answer):
        r = self.session.post(self.api_urls['answer'], data={
            'answer_value': answer,
            'question_id': qid,
            'csrfmiddlewaretoken': self.csrftoken
        }, headers=dict(Referer=self.api_urls['answer']))
        return r

    async def _cancel_last(self):
        # Remove the choices from last message
        if self._editor:
            await self._editor.editMessageReplyMarkup(reply_markup=None)
            self._editor = None
            self._edit_msg_ident = None

    async def on_chat_message(self, msg):
        if self.started:
            if not self.expecting_answer_to:
                # stray text message
                r = self.session.post(self.api_urls['message'], data={
                    'text': msg['text'],
                    'msgid': msg['message_id'],
                    'csrfmiddlewaretoken': self.csrftoken
                }, headers=dict(Referer=self.api_urls['message']))
            else:
                # response to a question that expected an answer
                r = self._send_answer(self.expecting_answer_to, msg['text'].strip())
                self.expecting_answer_to = None
            jsondata = json.loads(r.text)
            await self._process_response_bits(jsondata['bits'])
        else:
            try:
                # welcome and register user
                user_info = msg['from']

                # see if user ID is registered already
                response = requests.post('http://localhost:23000/private/get-telegram-user/', data={'id': user_info['id']})
                print(response.text)
                rdata = json.loads(response.text)
                if rdata['found']:
                    r = self.session.get(self.api_urls['start'], params={
                        'telegram_id': rdata['id']}
                    )
                    jsondata = json.loads(r.text)
                    await self._process_response_bits(jsondata['bits'])
                else:
                    text = "Hi {}! Welcome to the {} campaign!".format(user_info['first_name'], self.campaign)
                    await self.sender.sendMessage(text, parse_mode="Markdown")
                    text = 'I am now registering you in our system.'
                    await self.sender.sendMessage(text, parse_mode="Markdown")

                    # generate password
                    from utils import random_password
                    username = user_info['username']
                    password = random_password(10)
                    response = requests.post('http://localhost:23000/private/register-telegram-user/', data={
                        'id': user_info['id'],
                        'username': username,
                        'password': password,
                    })
                    result = json.loads(response.text)

                    text = 'In case you also try the web version of this campaign, you can log in with the username "{}" and password "{}".'.format(username, password)
                    await self.sender.sendMessage(text, parse_mode="Markdown")
                    text = 'There is no need to log in here, though! You\'re all set to start.'
                    await self.sender.sendMessage(text, parse_mode="Markdown")
                    text = 'First, let\'s do a quick tutorial.'
                    await self.sender.sendMessage(text, parse_mode="Markdown")
                    r = self.session.get(self.api_urls['tutorial'])
                    self.remaining_tutorial_bits = json.loads(r.text)['bits']
                    await self._process_response_bits(self.remaining_tutorial_bits)
            except requests.exceptions.ConnectionError:
                text = "Oh my, I can't contact the Taskforce servers. You'll have to try sometime later. I'm really sorry for this."
                await self.sender.sendMessage(text, parse_mode="Markdown")
                raise

            self.started = True

    async def on_callback_query(self, msg):
        query_id, from_id, query_data = glance(msg, flavor='callback_query')
        question_id, answer_value = msg['data'].split(DATA_SEPARATOR)
        print(question_id)
        print(answer_value)
        if question_id == "tutorial":
            # tutorial question answered, resume tutorial
            # get "next" bit and remove question from remaining bits
            q = self.remaining_tutorial_bits[0]
            for c in q['choices']:
                if str(c['label'].lower()) == str(answer_value):
                    self.remaining_tutorial_bits.insert(1, c['next'])
                    self.remaining_tutorial_bits.pop(0)
            await self._cancel_last()
            await self._process_response_bits(self.remaining_tutorial_bits)
        elif question_id == "idle":
            r = self.session.post(self.api_urls['message'], data={
                'text': msg['text'],
                'msgid': msg['message_id'],
                'csrfmiddlewaretoken': self.csrftoken
            }, headers=dict(Referer=self.api_urls['message']))
        else:
            r = self._send_answer(question_id, answer_value)
            jsondata = json.loads(r.text)
            await self._cancel_last()
            await self._process_response_bits(jsondata['bits'])
            # self.close()

    async def on__idle(self, event):
        await self._cancel_last()
        bit = {
            "type": "question",
            "id": "idle",
            "text": "Hey, thank you so much for your help! I'll be here if you want to get back to answering some questions!",
            "choices": [{
                "label": "Let's get back to it!",
                "value": "start"
            }]
        }
        text = emoji.emojize(bit['text'], use_aliases=True)
        keyboard = build_inline_keyboard(bit['id'], bit['choices'])
        sent = await self.sender.sendMessage(text, parse_mode="Markdown", reply_markup=keyboard)
        self._editor = telepot.aio.helper.Editor(self.bot, sent)
        self._edit_msg_ident = message_identifier(sent)
        self.close()

    def on_close(self, ex):
        pass


async def feeder(request):
    '''Forwards an incoming Telegram message to the message handler.'''
    campaign = request.match_info.get('campaign', None)
    data = await request.text()
    if bots[campaign].enabled:
        webhooks[campaign].feed(data)
        return web.Response(body='OK'.encode('utf-8'))
    else:
        return web.Response(body='Bot not enabled, ignoring message.'.encode('utf-8'))


async def do_start(request):
    '''Sets a bot to active. This is the default state.'''
    campaign = request.match_info.get('campaign', None)
    print("-- start {}".format(campaign))
    bots[campaign].enabled = True
    return web.Response(body='OK'.encode('utf-8'))


async def do_stop(request):
    '''Sets a bot to inactive, meaning that webhook calls won't be forwarded to Telegram.'''
    campaign = request.match_info.get('campaign', None)
    print("-- stop {}".format(campaign))
    bots[campaign].enabled = False
    return web.Response(body='OK'.encode('utf-8'))


async def do_status(request):
    '''Returns a list of the registered bots.'''
    print("-- status")
    entries = []
    for campaign in bots:
        bot = bots[campaign]
        status = {"campaign": bot.campaign,
                  "enabled": bot.enabled,
                  "token": bot.token,
                  }
        entries.append(status)
    return web.json_response(body=json.dumps(entries))


async def add_bot(campaign, token):
    '''Add a new bot to the running bots.'''
    loop = asyncio.get_event_loop()
    bot = telepot.aio.DelegatorBot(token, [
        include_callback_query_chat_id(
            pave_event_space())(
                per_chat_id(), create_open, TaskforceBot, timeout=600)],
        loop=loop)
    webhook = OrderedWebhook(bot)
    # set initial bot attributes
    bot.campaign = campaign
    bot.enabled = True
    bot.token = token
    # register the bot and webhook
    bots[campaign] = bot
    webhooks[campaign] = webhook
    # register the webhook with Telegram servers and start it
    url = "https://taskforce.jplusplus.org/telegram/" + campaign
    await bot.setWebhook(url)
    loop.create_task(webhook.run_forever())
    # asyncio.ensure_future(webhook.run_forever(), loop=loop)


async def do_add_bot(request):
    '''Add a new bot to the running bots.
    This is the endpoint handler, the actual work is done in add_bot.
    Called on the /add endpoint.'''
    data = await request.text()
    data = json.loads(data)
    print("-- add {}".format(data['campaign']))
    await add_bot(data['campaign'], data['token'])
    # get bot data from Telegram
    bot = telepot.Bot(data['token'])
    bot_info = bot.getMe()
    data['id'] = bot_info['id']
    data['first_name'] = bot_info['first_name']
    data['username'] = bot_info['username']
    return web.json_response(data)


async def do_remove_bot(request):
    '''Delete a bot from the running bots.
    Called on the /remove endpoint.'''
    data = await request.text()
    data = json.loads(data)
    campaign = data['campaign']
    print("-- remove {}".format(campaign))
    if bots.get(campaign):
        await bots[campaign].deleteWebhook()
        del bots[campaign]
        # del webhooks[campaign]
        return web.json_response(data)
    return web.json_response({'result': 'No such campaign: ' + campaign})


bot_info = requests.get('http://localhost:23000/private/get-campaigns/').text
bot_data = json.loads(bot_info)

# general instances
loop = asyncio.get_event_loop()
app = web.Application(loop=loop)
bots = {}
webhooks = {}

# per-bot instances
for entry in bot_data:
    loop.run_until_complete(add_bot(entry['campaign'], entry['token']))

# set up private endpoints
app.router.add_route('GET', '/telegram/{campaign}', feeder)
app.router.add_route('POST', '/telegram/{campaign}', feeder)
app.router.add_route('GET', '/start/{campaign}', do_start)
app.router.add_route('GET', '/stop/{campaign}', do_stop)

app.router.add_route('GET', '/status', do_status)
app.router.add_route('POST', '/add', do_add_bot)
app.router.add_route('POST', '/remove', do_remove_bot)


try:
    web.run_app(app, port=9500)
except KeyboardInterrupt:
    pass
