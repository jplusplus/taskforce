$(document).ready(function(){
    // Public site slide   
    $('.public-site-detail').slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: true,
      fade: true,
      asNavFor: '.public-site-nav',
      accessibility: true,
      adaptiveHeight: true
    });
    $('.public-site-nav').slick({
      slidesToShow: 4,
      slidesToScroll: 1,
      asNavFor: '.public-site-detail',
      dots: true,
      centerMode: true,
      focusOnSelect: true,
      accessibility: true
    });
    
    // Admin slide
    $('.admin-detail').slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: true,
      fade: true,
      asNavFor: '.admin-nav',
      accessibility: true,
      adaptiveHeight: true
    });
    $('.admin-nav').slick({
      slidesToShow: 3,
      slidesToScroll: 1,
      asNavFor: '.admin-detail',
      dots: true,
      centerMode: true,
      focusOnSelect: true,
      accessibility: true
    });
    
    
    // Chat slide
    $('.chat-detail').slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: true,
      fade: true,
      asNavFor: '.chat-nav',
      accessibility: true,
      adaptiveHeight: true
    });
    $('.chat-nav').slick({
      slidesToShow: 3,
      slidesToScroll: 1,
      asNavFor: '.chat-detail',
      dots: true,
      centerMode: true,
      focusOnSelect: true,
      accessibility: true
    });

});
