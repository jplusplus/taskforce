
There is an open (but unofficial) database of Portuguese MP's which has been extracted from the official [Portuguese Parliament](http://www.parlamento.pt) website.

And while the dataset is fairly complete, there is one important detail missing: each MP's gender!

It's pretty hard and error-prone to try to automatically determine the gender based on a person's picture or given name, which is why we need your help. You'll be presented with pictures of Portuguese MP's, and all that's needed is for you to tell us whether you're looking at a woman or a man.

_This campaign is a collaboration with [Transparência Hackday Portugal](http://transparenciahackday.org) and [Central de Dados](http://centraldedados.pt)._
