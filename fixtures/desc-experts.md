Understanding what a news item is talking about is fundamental for providing
good tools for journalists and their audiences. However, it is not
straightforward to automatically determine such information from a set of
articles.

An algorithm to automatically extract that kind of information is in
development, and we could really use your help in pointing out whether we got
the answers right. You'll be presented with a text snippet, along with our
guess as to what that snippet is talking about. You just need to tell us
whether we got it right.

_Note that this is a test campaign, not yet ready for public use._

_This campaign is a collaboration with [J++ Stockholm](http://jplusplus.org)._


