OCR (Optical Character Recognition) is a fantastic technology that enables
computers to automatically read words from scanned images and convert them to
text that we can then read and search. However, even the best OCR algorithms
stumble with certain issues like dirty pages and rough scans. 

In this campaign, we ask you to review words and the text that was identified
by an OCR algorithm, and tell us whether the algorithm got it right -- and if
not, what the correct answer should be. The texts come from Marchiver's trove
of historical publications, and your input will help improve their corpus.

_This campaign is a collaboration with the
[Marchiver](http://www.marchiver.com) project.


